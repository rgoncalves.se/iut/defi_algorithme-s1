/**
 * @author Evan DUBOIS | Romain GONCALVES
 * @version 1.0
 * @see http://info.iut-bm.univ-fcomte.fr/staff/perrot/DUT-INFO/S1/DEFI/index.html
 *
 * Programme final pour le défi d'algorithme en Java de S1
 * Manipule et génère des fichiers audio,
 * avec différents effets spéciaux.
 *
 */

import java.awt.image.SampleModel;
import java.util.concurrent.CompletionException;
import java.util.*;

import static java.lang.System.exit;
import static java.lang.System.out;

public class Povocoder {

	// Processing SEQUENCE size (100 msec with 44100Hz samplerate)
	final static int SEQUENCE = StdAudio.SAMPLE_RATE/10;
	// Overlapping size (20 msec)
	final static int OVERLAP = SEQUENCE/5 ;
	// Best OVERLAP offset seeking window (15 msec)
	final static int SEEK_WINDOW = 3*OVERLAP/4;

	public static void main(String[] args) {
		if (args.length < 2)
		{
			System.out.println("usage: povocoder input.wav freqScale\n");
			exit(1);
		}

		try
		{
			String wavInFile = args[0];
			double freqScale = Double.valueOf(args[1]);
			String outPutFile= wavInFile.split("\\.")[0] + "_" + freqScale +"_";


			// Open input .wev file
			double[] inputWav = StdAudio.read(wavInFile);

			// Resample test
			double[] newPitchWav = resample(inputWav, freqScale);
			StdAudio.save(outPutFile+"Resampled.wav", newPitchWav);

			// Simple dilatation
			double[] outputWav   = vocodeSimple(newPitchWav, 1.0/freqScale);
			StdAudio.save(outPutFile+"Simple.wav", outputWav);

			// Simple dilatation with overlaping
			outputWav = vocodeSimpleOver(newPitchWav, 1.0/freqScale);
			StdAudio.save(outPutFile+"SimpleOver.wav", outputWav);

			// Simple dilatation with overlaping and maximum cross correlation search
			outputWav = vocodeSimpleOverCross(newPitchWav, 1.0/freqScale);
			StdAudio.save(outPutFile+"SimpleOverCross.wav", outputWav);

			joue(outputWav);

			// Some echo above all
			outputWav = echo(outputWav, 100, 0.7);
			StdAudio.save(outPutFile+"SimpleOverCrossEcho.wav", outputWav);

		}
		catch (Exception e)
		{
			System.out.println("Error: "+ e.toString());
		}
	}

	// === === === === ===
	// METHODE : resample
	// === === === === ===
	/**
	 * Ré-échantillone un fichier audio donnée
	 *
	 * Ici, la durée réelle du signal de sortie vaut :
	 * (longueur d'entrée) / (freqScale) 
	 *
	 * @param input Tableau de double représentant le fichier source
	 * @param freqScale Fréquence de ré-échantillonage
	 */
	public static double[] resample(double[] input, double freqScale) {
		// Index utile dans le calcul du ré-échantillonage
		int inputIndex;
		// Taille du tableau en entrée
		int inputLong = input.length;
		// Taille du tableau en sortie, variant selon une fréquence donnée
		int outputLong = (int)(inputLong/freqScale);
		// Tableau en sortie, de taille différente qu'en entrée
		double output[] = new double [ outputLong ];

		if(freqScale == 1)
			return input;

		for(int i = 0; i < outputLong; i++) {
			// L'index d'entrée dépend de la fréquence comme coefficient
			// C'est grâce à ce dernier que l'on obtient l'effet sonore
			inputIndex = (int)(i*freqScale);
			// Ré-échantillonage plus aigu et plus court
			if(freqScale > 1)
				output[i] = input[inputIndex] * (freqScale - 1)/(freqScale);
			// Ré-échantillonage plus grave et plus long
			else
				output[i] = input[inputIndex] * (1 - freqScale)/(freqScale);
		}
		// Le tableau en sortie est d'une taille inversement
		// proportionelle à la fréquence
		return output;
	}

	// === === === ===
	// METHODE : echo
	// === === === ===
	/**
	 * Génère un effet d'écho sur une piste audio donnée
	 *
	 * Pas de calcul de durée de sortie nécessaire :
	 * On ne touche pas à la longueur du signal
	 *
	 * @param input Tableau de double représentant le signal sonore
	 * @param delayMs Retard de l'écho en millisecondes
	 * @param attn Atténuation de l'écho, compris dans [0.0-1.0]
	 * @see https://ptolemy.berkeley.edu/eecs20/week7/echo.html
	 */
	public static double[] echo(double[] input, double delayMs, double attn) {
		// Fréqence d'échantillonage
		final double FREQ = 44100;
		// Conversion de millisecondes en nombre d'indices
		delayMs = FREQ/1000 * delayMs;
		// Taille du tableau en entrée
		int inputLong = input.length;
		// Tableau de sortie, de même taille que le tableau d'entrée
		double output[] = new double [ inputLong ] ;
		// On applique l'écho sur chaque élément de la liste
		for(int i = 0; i < inputLong; i++) {
			// La valeur absolue est nécessaire,
			// afin de ne pas sortir des limites du tableau
			output[i] = input[i] + attn*output[(int)(Math.abs(i - delayMs))];
		}
		return output;
	}

	// === === === === === ===
	// METHODE : vocodeSimple
	// === === === === === ===
	/**
	 * Version minimale de l'algorithme de dilatation temporelle
	 *
	 * Ici, la durée réelle du signal de sortie vaut :
	 * (longueur d'entrée) / (timeScale) 
	 *
	 * @param input Tableau de double représentant le signal sonore
	 * @param timeScale Facteur de dilatation à appliquer au signal d'entrée
	 */
	public static double[] vocodeSimple(double[] input, double timeScale) {
		// Sortie de la méthode dans le cas particulier
		if(timeScale == 1) 
			return input;
		// Longueur du tableau en entrée
		final int inputLong = input.length;
		// Longueur du tabelau en sortie, comparable au tableau original
		int outputLong = (int)(inputLong / timeScale);
		// Déclaration du tableau en sortie
		double[] output = new double [ outputLong ];
		// Nombre total de séquences
		int totalSeq = Math.abs(inputLong - outputLong);
		int timeSeq = SEQUENCE;
		// Le nombre d'échantillons à supprimer entre chaques séquences
		int timeDel = totalSeq / timeSeq;
		int timeSaut = timeSeq + timeDel;
		// Longueurs de seq et saut
		int index = 0;
		// Dans le cas où le facteur de dilatation < 1
		if(timeScale > 1) {
			for(int i = 0; i < inputLong-timeSaut; i += timeSaut) {
				for(int j = i; j < i + timeSeq; j++) {
					output[(int)(j /timeScale)] = input[j];
				}
			}
		}
		// Dans le cas ou le facteur de dilatation > 1
		else {
			for(int i = 0; i < outputLong; i++) {
				if(index > timeSeq) {
					index++;
					if(index > timeSeq+timeDel) {
						index = 0;
					}
				}
				else {
					output[i] = input[(int)(i * timeScale)];
					index++;
				}
			}

		}
		return output;
	}

	// === === === === === === ===
	// METHODE : vocodeSimpleOver
	// === === === === === === ===
	/**
	 * Algorithme de dilatation temporelle
	 *
	 * Ici, la durée réelle du signal de sortie vaut :
	 * (longueur d'entrée) / (timeScale) 
	 *
	 * @param input Tableau de double représentant le signal sonore
	 * @param timeScale Facteur de dilatation à appliquer au signal d'entrée
	 */
	public static double[] vocodeSimpleOver(double[] input, double timeScale) {
		// Méthode non développée malgré des tests, 
		// car la méthode originale est incomplète
		return input;
	}

	// === === === === === === === ===
	// METHODE : vocodeSimpleOverCross
	// === === === === === === === ===
	/**
	 * Algorithme de dilatation temporelle
	 *
	 * Ici, la durée réelle du signal de sortie vaut :
	 * (longueur d'entrée) / (timeScale) 
	 *
	 * @param input Tableau de double représentant le signal sonore
	 * @param timeScale Facteur de dilatation à appliquer au signal d'entrée
	 */
	public static double[] vocodeSimpleOverCross(double[] input, double timeScale) {
		// Méthode non développée malgré des tests, 
		// car la méthode originale est incomplète
		return input;
	}

	// === === === ===
	// METHODE : joue
	// === === === ===
	/**
	 * Joue une fichier .wav en arrière-plan
	 * et affiche une visualisation du son
	 *
	 * @param input Tableau de double représentant le signal sonore
	 */
	public static void joue(double[] input) {

		// Initialisation Variables
		final int FREQ = 41100;
		final int XSTEPS = 1000;
		final int XSCALE = input.length;
		final double YSCALE = getMax(input);
		// Initialisation StdDraw
		StdDraw.setXscale(0, XSCALE);
		StdDraw.setYscale(-YSCALE, YSCALE);
		// Affichage grille
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.enableDoubleBuffering();
		for(int i = 0; i < XSCALE; i += FREQ) {
			StdDraw.line(i, YSCALE, i, -YSCALE);
		}
		StdDraw.show();
		StdDraw.disableDoubleBuffering();
		// Affichage signal + sortie audio
		StdDraw.setPenColor(StdDraw.BLUE);
		for(int i = 0; i < XSCALE; i += XSTEPS) {
			if(i+XSTEPS < XSCALE)
				StdDraw.line(i, input[i], i+XSTEPS, input[i+XSTEPS]);
			StdAudio.play(Arrays.copyOfRange(input, i, i+XSTEPS));
		}
	}
	// === === === === ===
	// METHODE : getMax
	// === === === === ===
	/**
	 * Retourne la valeur maxmimal d'un tableau de double
	 *
	 * @param input Tableau de double représentant le signal sonore
	 */
	public static double getMax(double[] input) {
		double max = 0;
		for(int i = 0; i < input.length; i++) {
			if(input[i] > max)
				max = input[i];
		}
		return max;
	}
}
